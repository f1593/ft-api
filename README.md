# Description 📝
Your friends are table football addicts. Unfortunately, they are also extremely bad at keeping score, which results in endless fights about who is the table football champion. To solve the situation once and for all, you decide to write a simple program that keeps track of the score during the game, stores the final result after the game ends, and displays some statistics.

# Structure 🏛
This component is a REST API implemented with NodeJS using KoaJS as Framework.

## Controllers
This directory contains all the controllers corresponding to a specific REST resource

## Middleware
This directory contains technical Koa middlewares

## Repositories
This directory contains repositories corresponding to database resources

## Routes
This directory contains routes middleware used by Koa for REST endpoints declaration

## Services
This directory contains all services of the application

## Validation
this directory contains the JOI validation for the endpoints

# Dependencies 🔗
- "@koa/cors": "^3.1.0", 
- "dayjs": "^1.10.4",
- "firebase-admin": "^9.7.0",
- "koa": "^2.13.1",
- "koa-bodyparser": "^4.3.0",
- "koa-cookie": "^1.0.0",
- "koa-joi-router": "^8.0.0",
- "uuid": "^8.3.2",
- "winston": "^3.3.3"

## DevDependencies: 
- "@types/jest": "^27.0.1",
- "eslint": "^7.25.0",
- "eslint-plugin-jest": "^24.4.0",
- "jest": "^27.0.6",
- "nodemon": "^2.0.7"

# How to run locally 🚀
This app is developped with NodeJS. You need to have node and npm installed in your computer. Execute the following steps for lunch the app locally

- npm install 
- npm run start

The server is running on the port 8080

# Demo environment 👀
This app is deployed on Google Cloud as a demo environment. For testing the API, you can go to https://editor.swagger.io/ and past the content of the file "swagger.yml". \
\
Now, you can choose between the demo env server or the local server and use the swagger to send request
