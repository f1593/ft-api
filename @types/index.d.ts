export interface Player {
  uuid: string;
  name: string;
  stats: Stats;
  type: "SOLO" | "TEAM";
}

export interface Stats {
  gamePlayed: number;
  wins: number;
  looses: number;
  goalFor: number;
  goalAgainst: number;
}

export interface Game {
  uuid: string;
  date: string;
  player1: Player;
  player2: Player;
  score1: number;
  score2: number;
  status: "IN_PROGRESS" | "FINISHED" | "CANCELED"
}

export enum GameAction {
  P1_SCORED,
  P2_SCORED,
  P1_UNSCORED,
  P2_UNSCORED,
  GAME_CANCELED,
  GAME_FINISED
}