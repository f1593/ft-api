FROM node:16-alpine3.11

WORKDIR /opt/app
ENTRYPOINT ["pm2-runtime", "pm2.yml"]

COPY . /opt/app
COPY build/pm2.yml /opt/app/pm2.yml

RUN npm install -g pm2 && \
    npm install --production
