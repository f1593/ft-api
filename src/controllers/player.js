const logger = require("../services/logger");

const playerRepository = require("../repositories/player");
const playerService = require("../services/player");
const { NotFoundError } = require("../middlewares/errors");

/**
 * Handle create player request
 * @param {import("koa").Context} ctx 
 * @param {*} next 
 */
const handlePostPlayer = (ctx, next) => {
  logger.info("handlePostPlayer");
  const player = {
    ...ctx.request.body,
    stats: playerService.emptyStats
  };
  return playerRepository.createPlayer(player)
    .then(res => {
      ctx.body = res;
      ctx.status = 200;
      return next();
    });
};

/**
 * Handle get all players request
 * @param {import("koa").Context} ctx 
 * @param {*} next 
 */
const handleGetPlayers = async (ctx, next) => {
  logger.info("handleGetPlayers");
  return playerRepository.getPlayers()
    .then(res => {
      ctx.body = res;
      ctx.status = 200;
      return next();
    });
};

/**
 * Handle get player request
 * @param {import("koa").Context} ctx 
 * @param {*} next 
 */
const handleGetPlayer = async (ctx, next) => {
  logger.info("handleGetPlayer");
  return playerRepository.getPlayer(ctx.request.params.uuid)
    .then(res => {
      if (res === null) throw new NotFoundError("Player not found");
      ctx.body = res;
      ctx.status = 200;
      return next();
    });
};

module.exports = {
  handlePostPlayer,
  handleGetPlayers,
  handleGetPlayer,
};