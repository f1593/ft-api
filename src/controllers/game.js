const logger = require("../services/logger");
const gameRepository = require("../repositories/game");
const gameService = require("../services/game");
const { NotFoundError } = require("../middlewares/errors");
const { addGameToStats } = require("../services/player");

/**
 * Handle create game request
 * @param {import("koa").Context} ctx 
 * @param {*} next 
 */
const handlePostGame = async (ctx, next) => {
  logger.info("handlePostGame");
  return gameRepository.createGame(ctx.request.body)
    .then(async res => {
      if(res.status === "FINISHED") {
        await addGameToStats(res);
      }
      return res;
    })
    .then(res => {
      ctx.body = res;
      ctx.status = 200;
      return next();
    });
};

/**
 * Handle get all games request
 * @param {import("koa").Context} ctx 
 * @param {*} next 
 */
const handleGetGames = async (ctx, next) => {
  logger.info("handleGetGames");
  const { player } = ctx.request.query;

  if(player) {
    return Promise.all([gameRepository.getAllGamesForPlayer(player, "player1"), 
      gameRepository.getAllGamesForPlayer(player, "player2")])
      .then(([p1, p2]) => {
        ctx.body = [...p1, ...p2];
        ctx.status = 200;
        return next();
      });
  } else {
    return gameRepository.getAllGames()
      .then(res => {
        ctx.body = res;
        ctx.status = 200;
        return next();
      });
  }
};

/**
 * Handle get game request
 * @param {import("koa").Context} ctx 
 * @param {*} next 
 */
const handleGetGame = async (ctx, next) => {
  logger.info("handleGetGame");
  return gameRepository.getGame(ctx.request.params.uuid)
    .then(res => {
      if(!res) throw new NotFoundError("Game not found");
      ctx.body = res;
      ctx.status = 200;
      return next();
    });
};

/**
 * Handle put game action request
 * @param {import("koa").Context} ctx 
 * @param {*} next 
 */
const handlePutGameAction = async (ctx, next) => {
  logger.info("handlePutGameAction");
  return gameRepository.getGame(ctx.request.params.uuid)
    .then(async res => {
      if(!res) throw new NotFoundError("Game not found");
      await gameService.handleGameActions(ctx.request.body.action, res);
      return handleGetGame(ctx, next);
    });
};


module.exports = {
  handlePostGame,
  handleGetGames,
  handleGetGame,
  handlePutGameAction
};
