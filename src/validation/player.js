const { Joi } = require("koa-joi-router");

const validatePostPlayer = {
  type: "json",
  body: {
    name: Joi.string().required(),
    type: Joi.string().required().valid("SOLO", "TEAM")
  }
};

module.exports = {
  validatePostPlayer
};