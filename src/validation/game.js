const { Joi } = require("koa-joi-router");

const validatePostGame = {
  type: "json",
  body: {
    date: Joi.string().required(),
    player1: Joi.string().required(),
    player2: Joi.string().required(),
    score1: Joi.number().required(),
    score2: Joi.number().required(),
    status: Joi.string().valid("IN_PROGRESS", "FINISHED")
  }
};

const validatePutGameAction = {
  type: "json",
  body: {
    action: Joi.string().required()
      .valid("P1_SCORED", "P2_SCORED", "P1_UNSCORED", "P2_UNSCORED", "GAME_CANCELED", "GAME_FINISHED")
  },
  path: {
    uuid: Joi.string().required()
  }
};

module.exports = {
  validatePostGame,
  validatePutGameAction
};