const { BadRequestError, errorHandler, NotFoundError, ConflictError } = require("./errors");

describe("errors middleware", () => {
  test("Should return BadRequestError with statusCode and body", async () => {
    const ctx = {};
    const err = new BadRequestError("BadRequest error");
    const next = () => Promise.reject(err);

    await errorHandler(ctx, next);

    expect(ctx.status).toBe(400);
    expect(ctx.body.status).toBe("error");
    expect(ctx.body.message).toBe("BadRequest error");
  });

  test("Should return NotFoundError with statusCode and body", async () => {
    const ctx = {};
    const err = new NotFoundError("NotFoundError error");
    const next = () => Promise.reject(err);

    await errorHandler(ctx, next);

    expect(ctx.status).toBe(404);
    expect(ctx.body.status).toBe("error");
    expect(ctx.body.message).toBe("NotFoundError error");
  });

  test("Should return ConflictError with statusCode and body", async () => {
    const ctx = {};
    const err = new ConflictError("ConflictError error");
    const next = () => Promise.reject(err);

    await errorHandler(ctx, next);

    expect(ctx.status).toBe(409);
    expect(ctx.body.status).toBe("error");
    expect(ctx.body.message).toBe("ConflictError error");
  });
});
