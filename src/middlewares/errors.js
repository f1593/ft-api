const logger = require("../services/logger");

class BadRequestError extends Error {
  constructor(message) {
    super(message);
    this.name = "BadRequestError";
    this.statusCode = 400;
  }
}

class NotFoundError extends Error {
  constructor(message) {
    super(message);
    this.name = "NotFoundError";
    this.statusCode = 404;
  }
}

class ConflictError extends Error {
  constructor(message) {
    super(message);
    this.name = "ConflictError";
    this.statusCode = 409;
  }
}


/**
 * Handle unhandled errors
 * @param {import("koa").Context} ctx 
 * @param {*} next 
 */
const errorHandler = (ctx, next) => {

  return next().catch(err => {
    const { message, statusCode } = err;
    ctx.type = "json";
    ctx.status = statusCode || 500;
    ctx.body = { status: "error", message };

    if(ctx.status >= 500){
      logger.error(message);
    }
  });
};

module.exports = {
  BadRequestError,
  NotFoundError,
  ConflictError,
  errorHandler,
};
