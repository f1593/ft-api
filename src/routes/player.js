const router = require("koa-joi-router");

const { handlePostPlayer, handleGetPlayers, handleGetPlayer } = require("../controllers/player");
const { validatePostPlayer } = require("../validation/player");

const playerRouter = router();
playerRouter.prefix("/api/v1/player");

playerRouter.route({
  method: "post",
  path: "/",
  handler: handlePostPlayer,
  validate: validatePostPlayer,
});

playerRouter.get("/", handleGetPlayers);
playerRouter.get("/:uuid", handleGetPlayer);

module.exports = playerRouter;