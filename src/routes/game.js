const router = require("koa-joi-router");
const { handlePostGame, handleGetGames, handleGetGame, handlePutGameAction } = require("../controllers/game");
const { validatePostGame, validatePutGameAction } = require("../validation/game");

const gameRouter = router();
gameRouter.prefix("/api/v1/game");

gameRouter.route({
  method: "post",
  path: "/",
  handler: handlePostGame,
  validate: validatePostGame,
});

gameRouter.route({
  method: "put",
  path: "/:uuid/action",
  handler: handlePutGameAction,
  validate: validatePutGameAction,
});

gameRouter.get("/", handleGetGames);
gameRouter.get("/:uuid", handleGetGame);

module.exports = gameRouter;