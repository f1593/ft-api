const Koa = require("koa");
const bodyParser = require("koa-bodyparser");
const cors = require("@koa/cors");
const { corsConf } = require("./services/config");
const logger = require("./services/logger");
const admin = require("firebase-admin");

admin.initializeApp();

const app = new Koa();

const playerRouter = require("./routes/player");
const gameRouter = require("./routes/game");
const { errorHandler } = require("./middlewares/errors");

app.use(errorHandler);
app.use(logger.middleware);
app.use(cors(corsConf));
app.use(bodyParser());
app.use(playerRouter.middleware());
app.use(gameRouter.middleware());

// eslint-disable-next-line no-undef
const port = process.env.PORT || 8080;
app.listen(port);
logger.info(`Server is listening on port ${port}`);