const game = {
  uuid: "wvujTOjIevgcIE1Z5q3i",
  score1: 7,
  player2: "Hxo0NkthqfYOxRiUyOgN",
  score2: 4,
  date: "2021-11-25 11:00",
  player1: "6H722x4bZFHdUdcX52uO",
  status: "IN_PROGRESS"
};

const player1 = {
  uuid: "6H722x4bZFHdUdcX52uO",
  type: "TEAM",
  name: "Fred & Chris",
  stats: {
    goalFor: 12,
    looses: 0,
    wins: 1,
    gamePlayed: 1,
    goalAgainst: 8
  }
};

const player2 = {
  uuid: "Hxo0NkthqfYOxRiUyOgN",
  stats: {
    wins: 0,
    goalAgainst: 12,
    goalFor: 8,
    gamePlayed: 1,
    looses: 1
  },
  name: "Peter & Mark",
  type: "TEAM"
};

const getPlayerMock = (uuid) => {
  if(uuid === "6H722x4bZFHdUdcX52uO") return player1;
  if(uuid === "Hxo0NkthqfYOxRiUyOgN") return player2;
};

const mockUpdatePlayer = jest.fn();

jest.mock("../repositories/player", () => ({
  __esModule: true,
  getPlayer: jest.fn().mockImplementation(getPlayerMock),
  updatePlayer: mockUpdatePlayer
}));

const playerService = require("./player");

describe("Player service tests", () => {
  it("emptyStats should return init stats", () => {
    const stats = playerService.emptyStats;

    expect(stats.gamePlayed).toBe(0);
    expect(stats.wins).toBe(0);
    expect(stats.looses).toBe(0);
    expect(stats.goalFor).toBe(0);
    expect(stats.goalAgainst).toBe(0);
  });

  it("addGameToStats should update players stats", async () => {
    await playerService.addGameToStats(game);

    expect(mockUpdatePlayer).toHaveBeenCalledWith({ "uuid":"6H722x4bZFHdUdcX52uO","type":"TEAM","name":"Fred & Chris","stats":{ "goalFor":19,"looses":0,"wins":2,"gamePlayed":2,"goalAgainst":12 } });
    expect(mockUpdatePlayer).toHaveBeenCalledWith({ "uuid":"Hxo0NkthqfYOxRiUyOgN","stats":{ "wins":0,"goalAgainst":19,"goalFor":12,"gamePlayed":2,"looses":2 },"name":"Peter & Mark","type":"TEAM" });
  });

  it("removeGameToStats should update players stats", async () => {
    await playerService.removeGameFromStats(game);

    expect(mockUpdatePlayer).toHaveBeenCalledWith({ "uuid":"6H722x4bZFHdUdcX52uO","type":"TEAM","name":"Fred & Chris","stats":{ "goalFor":12,"looses":0,"wins":1,"gamePlayed":1,"goalAgainst":8 } });
    expect(mockUpdatePlayer).toHaveBeenCalledWith({ "uuid":"Hxo0NkthqfYOxRiUyOgN","stats":{ "wins":0,"goalAgainst":12,"goalFor":8,"gamePlayed":1,"looses":1 },"name":"Peter & Mark","type":"TEAM" });
  });
});