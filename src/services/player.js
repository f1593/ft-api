const { getPlayer, updatePlayer } = require("../repositories/player"); 

const emptyStats = {
  gamePlayed: 0,
  wins: 0,
  looses: 0,
  goalFor: 0,
  goalAgainst: 0
};

/**
 * 
 * @param {import("../../@types").Game} game 
 */
const addGameToStats = async (game) => {
  const { score1, score2, player1, player2 } = game;
  const p1 = await getPlayer(player1);
  const p2 = await getPlayer(player2);
  
  p1.stats.gamePlayed++;
  p1.stats.goalFor = p1.stats.goalFor + score1;
  p1.stats.goalAgainst = p1.stats.goalAgainst + score2;

  p2.stats.gamePlayed++;
  p2.stats.goalFor = p2.stats.goalFor + score2;
  p2.stats.goalAgainst = p2.stats.goalAgainst + score1;

  if (score1 > score2) {
    p1.stats.wins++;
    p2.stats.looses++;
  }
  if (score1 < score2) {
    p2.stats.wins++;
    p1.stats.looses++;
  }
  await Promise.all([updatePlayer(p1), updatePlayer(p2)]);
};

/**
 * Remove game from stats
 * @param {import("../../@types").Game} game 
 */
const removeGameFromStats = async (game) => {
  const { score1, score2, player1, player2 } = game;
  const p1 = await getPlayer(player1);
  const p2 = await getPlayer(player2);
  

  p1.stats.gamePlayed--;
  p1.stats.goalFor = p1.stats.goalFor - score1;
  p1.stats.goalAgainst = p1.stats.goalAgainst - score2;

  p2.stats.gamePlayed--;
  p2.stats.goalFor = p2.stats.goalFor - score2;
  p2.stats.goalAgainst = p2.stats.goalAgainst - score1;

  if (score1 > score2) {
    p1.stats.wins--;
    p2.stats.looses--;
  }
  if (score1 < score2) {
    p2.stats.wins--;
    p1.stats.looses--;
  }
  await Promise.all([updatePlayer(p1), updatePlayer(p2)]);
};

module.exports = {
  emptyStats,
  addGameToStats,
  removeGameFromStats
};
