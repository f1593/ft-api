jest.mock("../repositories/game", () => ({
  __esModule: true,
  updateGame: jest.fn().mockImplementation(() => {})
}));
jest.mock("./player", () => ({
  __esModule: true,
  addGameToStats: jest.fn().mockImplementation(() => {})
}));
const gameService = require("./game");

const game = {
  uuid: "wvujTOjIevgcIE1Z5q3i",
  score1: 7,
  player2: "Hxo0NkthqfYOxRiUyOgN",
  score2: 4,
  date: "2021-11-25 11:00",
  player1: "6H722x4bZFHdUdcX52uO",
  status: "FINISHED"
};

describe("Game service tests", () => {
  it("Already finished game should not be updated", async () => {
    await gameService.handleGameActions("P1_SCORED", game).catch(err => expect(err.statusCode).toBe(409));
    await gameService.handleGameActions("P2_SCORED", game).catch(err => expect(err.statusCode).toBe(409));
    await gameService.handleGameActions("P1_UNSCORED", game).catch(err => expect(err.statusCode).toBe(409));
    await gameService.handleGameActions("P2_UNSCORED", game).catch(err => expect(err.statusCode).toBe(409));
  });

  it("P1_SCORED should increment score1", async () => {
    game.status = "IN_PROGRESS";
    game.score1 = 7;

    await gameService.handleGameActions("P1_SCORED", game);

    expect(game.score1).toBe(8);
  });

  it("P1_UNSCORED should decrement score1", async () => {
    game.status = "IN_PROGRESS";
    game.score1 = 7;

    await gameService.handleGameActions("P1_UNSCORED", game);

    expect(game.score1).toBe(6);
  });

  it("P2_SCORED should increment score2", async () => {
    game.status = "IN_PROGRESS";
    game.score2 = 4;

    await gameService.handleGameActions("P2_SCORED", game);

    expect(game.score2).toBe(5);
  });

  it("P2_UNSCORED should decrement score2", async () => {
    game.status = "IN_PROGRESS";
    game.score2 = 4;

    await gameService.handleGameActions("P2_UNSCORED", game);

    expect(game.score2).toBe(3);
  });

  it("GAME_CANCELED should update game status", async () => {
    game.status = "IN_PROGRESS";

    await gameService.handleGameActions("GAME_CANCELED", game);

    expect(game.status).toBe("CANCELED");
  });

  it("GAME_FINISHED should update game status", async () => {
    game.status = "IN_PROGRESS";

    await gameService.handleGameActions("GAME_FINISHED", game);

    expect(game.status).toBe("FINISHED");
  });
});