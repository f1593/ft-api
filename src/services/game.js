const { ConflictError } = require("../middlewares/errors");
const { updateGame } = require("../repositories/game");
const { removeGameFromStats, addGameToStats } = require("./player");

/**
 * 
 * @param {import("../../@types").GameAction} action 
 * @param {import("../../@types").Game} game 
 */
const handleGameActions = async (action, game) => {
  switch(action) {
  case "P1_SCORED":
    canBeModified(game);
    game.score1++;
    await updateGame(game);
    break;
  case "P2_SCORED":
    canBeModified(game);
    game.score2++;
    await updateGame(game);
    break;
  case "P1_UNSCORED":
    canBeModified(game);
    game.score1--;
    await updateGame(game);
    break;
  case "P2_UNSCORED":
    canBeModified(game);
    game.score2--;
    await updateGame(game);
    break;
  case "GAME_CANCELED":
    await cancelGame(game);
    break;
  case "GAME_FINISHED":
    await finishGame(game);
    break;
  default:
    break;
  }
};

/**
 * Check if the game is open to be modified
 * @param {import("../../@types").Game} game 
 */
const canBeModified = (game) => {
  if(game.status === "CANCELED" || game.status === "FINISHED") {
    throw new ConflictError("Action can't be performed on this state");
  }
};

/**
 * Cancel a game
 * @param {import("../../@types").Game} game 
 */
const cancelGame = async (game) => {
  if(game.status === "FINISHED") await removeGameFromStats(game);
  game.status = "CANCELED";
  return updateGame(game);
};

/**
 * Finish a game
 * @param {import("../../@types").Game} game 
 */
const finishGame = async (game) => {
  await addGameToStats(game);
  game.status = "FINISHED";
  return updateGame(game);
};

module.exports = {
  handleGameActions
};