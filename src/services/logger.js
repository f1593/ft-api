const { createLogger, format, transports } = require("winston");
const dayjs = require("dayjs");
const config = require("./config");
 
const { combine } = format;
 
// Used for local dev only
const textLogger = createLogger({
  level: config.logger.level,
  transports: [new transports.Console({
    format: format.combine(
      format.colorize(),
      format.printf(info => `${dayjs().format("YYYY-MM-DD HH:mm:ss")} - [${info.level}] ${info.message}${info.stacktrace ? `\n${info.stacktrace}` : ""}`),
    ),
  },
  )],
});
 
const jsonLogger = createLogger({
  format: combine(
    format.timestamp({ format: "YYYY-MM-DD HH:mm:ss.sss" }),
    format.json(),
  ),
  transports: [new transports.Console()],
});
 
const logger = process.env.NODE_ENV === "production" ? jsonLogger : textLogger;

logger.middleware = (ctx, next) => {
  ctx.logger = logger;
  return next();
};
 
module.exports = logger;