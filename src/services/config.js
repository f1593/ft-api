
const env = process.env.NODE_ENV || "dev";

const config = require(`../../config/${env}/config.json`);

process.env.GOOGLE_APPLICATION_CREDENTIALS = `config/${env}/gcp-credentials.json`;

const corsOrigin = (ctx) => {
  if (config.corsConf.origins.includes(ctx.headers.origin)) return ctx.headers.origin;
  return config.corsConf.origins[0];
};

module.exports = {
  ...config,
  corsConf: {
    origin: corsOrigin,
    credentials: true,
  }
};