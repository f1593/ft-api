const { addDocument, getDocuments, getDocument, updateDocument, firestore } = require("./firestore");

/**
 * Create a new Player in the collection
 * @param {import("../../@types").Game} game 
 * @returns {Promise<import("../../@types").Game>}
 */
const createGame = (game) => addDocument("games", game);

/**
 * Get all games
 * @returns {Promise<import("../../@types").Game[]>}
 */
const getAllGames = () => getDocuments("games");

/**
 * Get All games for a player
 * @param {string} uuid 
 * @param {"player1" | "player2"} field select player field
 * @returns {Promise<import("../../@types").Game[]>}
 */
const getAllGamesForPlayer = (uuid, field) => firestore.collection("games")
  .where(field, "==", uuid)
  .get()
  .then((querySnapshot) => 
    querySnapshot.docs.map(doc => ({
      uuid: doc.id,
      ...doc.data()
    })));

/**
 * Get a game by his uuid
 * @param {string} uuid 
 * @returns {Promise<import("../../@types").Game>}
 */
const getGame = (uuid) => getDocument("games", uuid);

/**
 * Update a game
 * @param {string} uuid 
 * @param {import("../../@types").Game} game 
 * @returns 
 */
const updateGame = (game) => {
  const uuid = game.uuid;
  delete game.uuid;
  return updateDocument("games", uuid, game);
};

module.exports = {
  createGame,
  getAllGames,
  getAllGamesForPlayer,
  getGame,
  updateGame
};
