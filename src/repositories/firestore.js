const firestore = require("firebase-admin").firestore();

/**
 * Add a new document in the collection
 * @param {string} col collection
 * @param {*} res resource
 * @returns 
 */
const addDocument = (col, res) => firestore.collection(col)
  .add(res)
  .then(ref => ref.get()).then(s => ({
    uuid: s.id,
    ...s.data()
  }));

/**
 * Get all resources in the collection
 * @param {string} col collection
 * @returns 
 */
const getDocuments = (col) => firestore.collection(col)
  .get()
  .then((querySnapshot) => 
    querySnapshot.docs.map(doc => ({
      uuid: doc.id,
      ...doc.data()
    })));

/**
 * Get a document by his UUID
 * @param {string} col collection
 * @param {string} uuid 
 * @returns 
 */
const getDocument = (col, uuid) => firestore.collection(col)
  .doc(uuid)
  .get()
  .then(doc => doc.data() ? ({ uuid: doc.id, ...doc.data() }) : null);

/**
 * Update a document
 * @param {string} col collection
 * @param {string} uuid uuid of the document
 * @param {*} data Data to update
 * @returns 
 */
const updateDocument = (col, uuid, data) => firestore.collection(col)
  .doc(uuid).update(data);

module.exports = {
  addDocument,
  getDocuments,
  getDocument,
  updateDocument,
  firestore
};
