const { addDocument, getDocument, getDocuments, updateDocument } = require("./firestore");

/**
 * Create a new Player in the collection
 * @param {import("../../@types").Player} player 
 * @returns {Promise<import("../../@types").Player>}
 */
const createPlayer = (player) => addDocument("players", player);

/**
 * Get all Players in the collection
 * @returns {Promise<import("../../@types").Player>[]}
 */
const getPlayers = () => getDocuments("players");

/**
 * Get a Player by his UUID
 * @param {string} uuid 
 * @returns {Promise<import("../../@types").Player>}
 */
const getPlayer = uuid => getDocument("players", uuid);

/**
 * Update player
 * @param {import("../../@types").Player} player 
 */
const updatePlayer = (player) => {
  const uuid = player.uuid;
  delete player.uuid;
  updateDocument("players", uuid, player);
};

module.exports = {
  createPlayer,
  getPlayers,
  getPlayer,
  updatePlayer
};
